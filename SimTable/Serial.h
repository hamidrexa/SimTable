﻿#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <QSerialPort>
// #include <QSerialPortInfo>

struct Settings {
	QString name;
	qint32 baudRate;
	QSerialPort::DataBits dataBits;
	QSerialPort::Parity parity;
	QSerialPort::StopBits stopBits;
	QSerialPort::FlowControl flowControl;
};

class Serial : public QObject
{
	Q_OBJECT
public :
	explicit Serial(QObject *parent = 0);
	~Serial();
	void initPort(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);  //write settings
	bool WriteToPort(QByteArray data); //write data
	void OpenPort();
	void ClosePort();
	int bytesAvailable();
	bool waitForBytesAvailable(int numBytes, int timeout);
 
signals:
	void FinishedPort();
	void DataReadyRead(QString);
	void PortError(QString error);
	void softResetResponse();
 
public slots :
	void PortProcess();
	void ReadDataPort();
	void ReadDataPort(int timeout);
	void serialDataSent(qint64 bytes);
	void softReset();
	void ErrorHandle(QSerialPort::SerialPortError error); //errors in transaction process
 
public:
	QSerialPort *thisPort;
 
private:
	Settings SettingsPort;	
	
};

#endif // SERIAL_H
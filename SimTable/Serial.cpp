#include <QTime>
#include <QThread>
#include <QDebug>
#include "Serial.h"



Serial::Serial(QObject *parent) 
	: QObject(parent)
	//, thisPort (this) 
{
	qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError"); //register meta type
	qDebug() << "Serial::Serial()" << "thread: " << QThread::currentThread();
	thisPort = new QSerialPort(this);
 
}


Serial::~Serial()
{ 
	qDebug() << "Serial::~Serial() - Begin" << "thread: " << QThread::currentThread();
	ClosePort();
	emit FinishedPort();
	disconnect(thisPort, SIGNAL(readyRead()), this, SLOT(ReadDataPort()));
	//disconnect(thisPort, SIGNAL(bytesWritten(qint64)), this, SLOT(serialDataSent(qint64)));
	disconnect(thisPort, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(ErrorHandle(QSerialPort::SerialPortError)));
	thisPort->deleteLater();
	qDebug() << "Serial::~Serial() - End" << "thread: " << QThread::currentThread();
}

void Serial::PortProcess()
{
	qDebug() << "Serial::PortProcess()" << thisPort;
	connect(thisPort, SIGNAL(readyRead()), this, SLOT(ReadDataPort()));
	//connect(thisPort, SIGNAL(bytesWritten(qint64)), this, SLOT(serialDataSent(qint64)));
	connect(thisPort, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(ErrorHandle(QSerialPort::SerialPortError)));
}
 
void Serial::initPort(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl)
{
	SettingsPort.name = name; //com port name
	SettingsPort.baudRate = (QSerialPort::BaudRate)baudrate; //set baudrate
	SettingsPort.dataBits = (QSerialPort::DataBits)DataBits; //set bits in message
	SettingsPort.parity = (QSerialPort::Parity)Parity; //parity
	SettingsPort.stopBits = (QSerialPort::StopBits)StopBits; //stop bit
	SettingsPort.flowControl = (QSerialPort::FlowControl)FlowControl; //flow control
}
 
void Serial::OpenPort()
{
	thisPort->setPortName(SettingsPort.name);
	qDebug() << "Serial::open()1" << thisPort;
	if (thisPort->open(QIODevice::ReadWrite))
	{
		qDebug() << "Serial::open()2" << thisPort;
		if (thisPort->setBaudRate(QSerialPort::Baud115200)//(SettingsPort.baudRate)
		         && thisPort->setDataBits(QSerialPort::Data8)//(SettingsPort.dataBits)//DataBits
		         && thisPort->setParity(QSerialPort::NoParity)//(SettingsPort.parity)
		         && thisPort->setStopBits(QSerialPort::OneStop)//(SettingsPort.stopBits)
		         && thisPort->setFlowControl(QSerialPort::NoFlowControl))//(SettingsPort.flowControl))
		{
			//thisPort->setReadBufferSize(1000);
			if (!thisPort->isOpen())
			{
				thisPort->close();
				emit PortError(thisPort->errorString().toLocal8Bit());
			}
		}
	}
	else
	{
		qDebug() << "Serial::open()3" << thisPort->errorString().toLocal8Bit();
		thisPort->close();
		emit PortError(thisPort->errorString().toLocal8Bit());
	}
}
 
/*
 * Close Serial Port
 * */
void Serial::ClosePort()
{
	if (thisPort->isOpen())
	{
		thisPort->close();
		return;
	}
	emit PortError("port not open");
}
 
 
/*
 * Write data to current serial port
 */
bool Serial::WriteToPort(QByteArray data)
{
	
	if (thisPort->isOpen())
	{
		if (thisPort->write(data) == data.length())
		{
			//thisPort->flush();
			if (thisPort->waitForBytesWritten(500))
			{
				qDebug() << "Serial::WriteToPort() " << "thisPort->waitForBytesWritten(500)" << QString(true);
				return true;
			}
			else
				return false;
		}
		else
			return false;
		
	}
	return false;
 
}
 
/*
 * Read data from port
 */
void Serial::ReadDataPort()
{
	QString inputData = 0;
 
	if (thisPort->isOpen())
	{
		while (thisPort->waitForReadyRead(500))
			inputData += QString(thisPort->readAll()); // readAll ; readLine
 
		qDebug() << "Serial::ReadDataPort()" << thisPort << "\n\r" << 
			"inputData" << inputData;
		
		if (inputData.isEmpty()) emit PortError("Serial read timeout");
		else emit DataReadyRead(inputData);
		
		//thisPort->flush();
		//thisPort->clear(QSerialPort::AllDirections);
		return;
	}
	emit PortError("port not open");
}

/*
 * Read data from port
 * Delay up to the specified delay ms for the first response byte,
 * then read response data until the number of ms specified in timeout
 * ellaspes between response bytes, then return response data.
 */
void Serial::ReadDataPort(int timeout)
{
	QByteArray inputData = 0;
 
	if (thisPort->isOpen())
	{
		if (waitForBytesAvailable(1, timeout)) {
			//Read first incoming data
			 inputData.append(thisPort->readAll());

			//Continue reading until timout expires
			while(waitForBytesAvailable(1, timeout)) {
				inputData.append(thisPort->readAll());
			}
		}
		if (inputData.isEmpty()) emit PortError("Read tomeout");
		else emit DataReadyRead(inputData);
		return;
	}
	emit PortError("port not open");
}

/*
 * Returns the number of bytes available at the port or -1 if the port is not open
 */
int Serial::bytesAvailable() {
	QTime stopWatch;
	if (!thisPort->isOpen())
	{
		return -1;
	} else {
		stopWatch.restart();
		thisPort->waitForReadyRead(1);
		int numBytes = thisPort->bytesAvailable();
		//qDebug() << "Took" << stopWatch.elapsed() << "ms" << "and found" << numBytes << "bytes";
		return numBytes;
	}
}

/*
 * Wait for the specified number of bytes to be avialable or timeout.
 * Returns true if the specified number of bytes are available, false otherwise.
 */
bool Serial::waitForBytesAvailable(int numBytes, int timeout) {

	QTime startTime = QTime::currentTime();
	QTime doneTime = startTime.addMSecs(timeout);
	while (QTime::currentTime() < doneTime) {
		if (bytesAvailable() >= numBytes) {
			return true;
		}
	}
	return false;
}
 

void Serial::serialDataSent(qint64 bytes)
{
	qDebug() << "MainWindow::serialDataSent" << bytes;
}

/*
 * Soft reset the serial port.
 * This funciton closes and reopens the serial port with the same settings.
 */
void Serial::softReset() {
	ClosePort();
	OpenPort();
	emit softResetResponse();
}


void Serial::ErrorHandle(QSerialPort::SerialPortError error)
{
	if ((thisPort->isOpen()) && (error == QSerialPort::ResourceError))
	{
		emit PortError(thisPort->errorString().toLocal8Bit());
		ClosePort();
	}
}
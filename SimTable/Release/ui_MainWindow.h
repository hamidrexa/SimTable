/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Operator;
    QAction *actionDebug;
    QAction *actionAbout;
    QAction *action_About;
    QAction *action_Conecct;
    QAction *action_Disconnect;
    QAction *action_Port_Configure;
    QWidget *centralWidget;
    QWidget *widget;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit;
    QLabel *label;
    QLabel *label_2;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_4;
    QWidget *widget_2;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_7;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_22;
    QLabel *label_6;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_19;
    QLineEdit *lineEdit_25;
    QLineEdit *lineEdit_17;
    QLineEdit *lineEdit_21;
    QLineEdit *lineEdit_8;
    QLineEdit *lineEdit_14;
    QLineEdit *lineEdit_18;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_11;
    QLineEdit *lineEdit_15;
    QLineEdit *lineEdit_23;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_20;
    QLineEdit *lineEdit_9;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_26;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_6;
    QLabel *label_8;
    QLabel *label_14;
    QLabel *label_9;
    QLabel *label_5;
    QLabel *label_12;
    QLabel *label_7;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_11;
    QLabel *label_10;
    QPushButton *pushButton_3;
    QFrame *line;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit_29;
    QPushButton *pushButton_6;
    QLabel *label_13;
    QLineEdit *lineEdit_28;
    QPushButton *pushButton_5;
    QLabel *label_15;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *lineEdit_27;
    QPushButton *pushButton_4;
    QFrame *line_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_16;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QMenu *menu_File;
    QMenu *menu_DebugMode;
    QMenu *menu_Connection;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(651, 674);
        MainWindow->setDockNestingEnabled(false);
        action_Operator = new QAction(MainWindow);
        action_Operator->setObjectName(QStringLiteral("action_Operator"));
        actionDebug = new QAction(MainWindow);
        actionDebug->setObjectName(QStringLiteral("actionDebug"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        action_About = new QAction(MainWindow);
        action_About->setObjectName(QStringLiteral("action_About"));
        action_Conecct = new QAction(MainWindow);
        action_Conecct->setObjectName(QStringLiteral("action_Conecct"));
        action_Disconnect = new QAction(MainWindow);
        action_Disconnect->setObjectName(QStringLiteral("action_Disconnect"));
        action_Port_Configure = new QAction(MainWindow);
        action_Port_Configure->setObjectName(QStringLiteral("action_Port_Configure"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 10, 611, 301));
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 30, 71, 23));
        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(30, 60, 75, 23));
        lineEdit_2 = new QLineEdit(widget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(72, 150, 91, 20));
        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(72, 120, 91, 20));
        lineEdit->setReadOnly(true);
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 120, 47, 13));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 150, 47, 13));
        verticalLayoutWidget = new QWidget(widget);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(189, 9, 411, 281));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        widget_2 = new QWidget(centralWidget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        widget_2->setGeometry(QRect(20, 320, 611, 301));
        layoutWidget = new QWidget(widget_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 601, 291));
        verticalLayout_3 = new QVBoxLayout(layoutWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetNoConstraint);
        lineEdit_3 = new QLineEdit(layoutWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout->addWidget(lineEdit_3, 1, 1, 1, 1);

        lineEdit_7 = new QLineEdit(layoutWidget);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));

        gridLayout->addWidget(lineEdit_7, 1, 2, 1, 1);

        lineEdit_13 = new QLineEdit(layoutWidget);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));

        gridLayout->addWidget(lineEdit_13, 1, 3, 1, 1);

        lineEdit_16 = new QLineEdit(layoutWidget);
        lineEdit_16->setObjectName(QStringLiteral("lineEdit_16"));

        gridLayout->addWidget(lineEdit_16, 1, 6, 1, 1);

        lineEdit_22 = new QLineEdit(layoutWidget);
        lineEdit_22->setObjectName(QStringLiteral("lineEdit_22"));

        gridLayout->addWidget(lineEdit_22, 1, 8, 1, 1);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 0, 1, 1, 1);

        lineEdit_10 = new QLineEdit(layoutWidget);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));

        gridLayout->addWidget(lineEdit_10, 1, 4, 1, 1);

        lineEdit_19 = new QLineEdit(layoutWidget);
        lineEdit_19->setObjectName(QStringLiteral("lineEdit_19"));

        gridLayout->addWidget(lineEdit_19, 1, 5, 1, 1);

        lineEdit_25 = new QLineEdit(layoutWidget);
        lineEdit_25->setObjectName(QStringLiteral("lineEdit_25"));

        gridLayout->addWidget(lineEdit_25, 1, 7, 1, 1);

        lineEdit_17 = new QLineEdit(layoutWidget);
        lineEdit_17->setObjectName(QStringLiteral("lineEdit_17"));

        gridLayout->addWidget(lineEdit_17, 2, 6, 1, 1);

        lineEdit_21 = new QLineEdit(layoutWidget);
        lineEdit_21->setObjectName(QStringLiteral("lineEdit_21"));

        gridLayout->addWidget(lineEdit_21, 2, 7, 1, 1);

        lineEdit_8 = new QLineEdit(layoutWidget);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));

        gridLayout->addWidget(lineEdit_8, 3, 2, 1, 1);

        lineEdit_14 = new QLineEdit(layoutWidget);
        lineEdit_14->setObjectName(QStringLiteral("lineEdit_14"));

        gridLayout->addWidget(lineEdit_14, 3, 3, 1, 1);

        lineEdit_18 = new QLineEdit(layoutWidget);
        lineEdit_18->setObjectName(QStringLiteral("lineEdit_18"));

        gridLayout->addWidget(lineEdit_18, 3, 6, 1, 1);

        lineEdit_24 = new QLineEdit(layoutWidget);
        lineEdit_24->setObjectName(QStringLiteral("lineEdit_24"));

        gridLayout->addWidget(lineEdit_24, 3, 8, 1, 1);

        lineEdit_11 = new QLineEdit(layoutWidget);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));

        gridLayout->addWidget(lineEdit_11, 2, 4, 1, 1);

        lineEdit_15 = new QLineEdit(layoutWidget);
        lineEdit_15->setObjectName(QStringLiteral("lineEdit_15"));

        gridLayout->addWidget(lineEdit_15, 2, 5, 1, 1);

        lineEdit_23 = new QLineEdit(layoutWidget);
        lineEdit_23->setObjectName(QStringLiteral("lineEdit_23"));

        gridLayout->addWidget(lineEdit_23, 2, 8, 1, 1);

        lineEdit_12 = new QLineEdit(layoutWidget);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));

        gridLayout->addWidget(lineEdit_12, 3, 4, 1, 1);

        lineEdit_20 = new QLineEdit(layoutWidget);
        lineEdit_20->setObjectName(QStringLiteral("lineEdit_20"));

        gridLayout->addWidget(lineEdit_20, 3, 5, 1, 1);

        lineEdit_9 = new QLineEdit(layoutWidget);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));

        gridLayout->addWidget(lineEdit_9, 2, 3, 1, 1);

        lineEdit_5 = new QLineEdit(layoutWidget);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        gridLayout->addWidget(lineEdit_5, 3, 1, 1, 1);

        lineEdit_26 = new QLineEdit(layoutWidget);
        lineEdit_26->setObjectName(QStringLiteral("lineEdit_26"));

        gridLayout->addWidget(lineEdit_26, 3, 7, 1, 1);

        lineEdit_4 = new QLineEdit(layoutWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        gridLayout->addWidget(lineEdit_4, 2, 1, 1, 1);

        lineEdit_6 = new QLineEdit(layoutWidget);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));

        gridLayout->addWidget(lineEdit_6, 2, 2, 1, 1);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 0, 3, 1, 1);

        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout->addWidget(label_14, 0, 6, 1, 1);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 0, 4, 1, 1);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout->addWidget(label_12, 0, 7, 1, 1);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 0, 2, 1, 1);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout->addWidget(label_11, 0, 8, 1, 1);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 0, 5, 1, 1);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        gridLayout->addWidget(pushButton_3, 4, 4, 1, 1);


        verticalLayout_3->addLayout(gridLayout);

        line = new QFrame(layoutWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        lineEdit_29 = new QLineEdit(layoutWidget);
        lineEdit_29->setObjectName(QStringLiteral("lineEdit_29"));

        gridLayout_2->addWidget(lineEdit_29, 1, 1, 1, 1);

        pushButton_6 = new QPushButton(layoutWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        gridLayout_2->addWidget(pushButton_6, 0, 2, 1, 1);

        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_2->addWidget(label_13, 0, 0, 1, 1);

        lineEdit_28 = new QLineEdit(layoutWidget);
        lineEdit_28->setObjectName(QStringLiteral("lineEdit_28"));

        gridLayout_2->addWidget(lineEdit_28, 0, 1, 1, 1);

        pushButton_5 = new QPushButton(layoutWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        gridLayout_2->addWidget(pushButton_5, 1, 2, 1, 1);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_2->addWidget(label_15, 1, 0, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        line_2 = new QFrame(layoutWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lineEdit_27 = new QLineEdit(layoutWidget);
        lineEdit_27->setObjectName(QStringLiteral("lineEdit_27"));
        lineEdit_27->setReadOnly(true);

        horizontalLayout->addWidget(lineEdit_27);

        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        line_3 = new QFrame(layoutWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        verticalLayout_2->addWidget(label_16);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setReadOnly(true);

        verticalLayout_2->addWidget(textEdit);


        horizontalLayout_2->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 651, 21));
        menu_File = new QMenu(menuBar);
        menu_File->setObjectName(QStringLiteral("menu_File"));
        menu_DebugMode = new QMenu(menuBar);
        menu_DebugMode->setObjectName(QStringLiteral("menu_DebugMode"));
        menu_Connection = new QMenu(menuBar);
        menu_Connection->setObjectName(QStringLiteral("menu_Connection"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        QWidget::setTabOrder(pushButton, pushButton_2);
        QWidget::setTabOrder(pushButton_2, textEdit);
        QWidget::setTabOrder(textEdit, lineEdit);
        QWidget::setTabOrder(lineEdit, lineEdit_2);

        menuBar->addAction(menu_File->menuAction());
        menuBar->addAction(menu_Connection->menuAction());
        menuBar->addAction(menu_DebugMode->menuAction());
        menu_File->addAction(action_Operator);
        menu_File->addAction(actionDebug);
        menu_DebugMode->addAction(actionAbout);
        menu_DebugMode->addAction(action_About);
        menu_Connection->addAction(action_Conecct);
        menu_Connection->addAction(action_Disconnect);
        menu_Connection->addSeparator();
        menu_Connection->addAction(action_Port_Configure);

        retranslateUi(MainWindow);
        QObject::connect(pushButton, SIGNAL(clicked()), MainWindow, SLOT(ButtonClicked()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "SimTable", 0));
        action_Operator->setText(QApplication::translate("MainWindow", "&Operator", 0));
        actionDebug->setText(QApplication::translate("MainWindow", "&Debug", 0));
        actionAbout->setText(QApplication::translate("MainWindow", "&Help", 0));
        action_About->setText(QApplication::translate("MainWindow", "&About", 0));
        action_Conecct->setText(QApplication::translate("MainWindow", "&Connect", 0));
        action_Disconnect->setText(QApplication::translate("MainWindow", "&Disconnect", 0));
        action_Port_Configure->setText(QApplication::translate("MainWindow", "&Port Configure", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Open Serial Port", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "Send Data", 0));
        label->setText(QApplication::translate("MainWindow", "Reference", 0));
        label_2->setText(QApplication::translate("MainWindow", "Output", 0));
        label_6->setText(QApplication::translate("MainWindow", "Output", 0));
        label_8->setText(QApplication::translate("MainWindow", "Output", 0));
        label_14->setText(QApplication::translate("MainWindow", "Output", 0));
        label_9->setText(QApplication::translate("MainWindow", "Output", 0));
        label_5->setText(QApplication::translate("MainWindow", "Output", 0));
        label_12->setText(QApplication::translate("MainWindow", "Output", 0));
        label_7->setText(QApplication::translate("MainWindow", "Output", 0));
        label_4->setText(QApplication::translate("MainWindow", "Output", 0));
        label_3->setText(QApplication::translate("MainWindow", "Output", 0));
        label_11->setText(QApplication::translate("MainWindow", "Output", 0));
        label_10->setText(QApplication::translate("MainWindow", "Output", 0));
        pushButton_3->setText(QApplication::translate("MainWindow", "Apply", 0));
        pushButton_6->setText(QApplication::translate("MainWindow", "Connect", 0));
        label_13->setText(QApplication::translate("MainWindow", "Com", 0));
        pushButton_5->setText(QApplication::translate("MainWindow", "Disconnect", 0));
        label_15->setText(QApplication::translate("MainWindow", "Baudrate", 0));
        pushButton_4->setText(QApplication::translate("MainWindow", "Send Data", 0));
        label_16->setText(QApplication::translate("MainWindow", "Received Data", 0));
        menu_File->setTitle(QApplication::translate("MainWindow", "&Mode", 0));
        menu_DebugMode->setTitle(QApplication::translate("MainWindow", "&About", 0));
        menu_Connection->setTitle(QApplication::translate("MainWindow", "&Connection", 0));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

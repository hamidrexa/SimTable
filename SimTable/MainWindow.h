#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Serial.h"
#include "Controller.h"
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
    
public :
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	QVector<double> parseData(QString data);
	void initUI();

protected slots :
	void ButtonClicked();
	void serialDataReceived(QString data);
	void updateUI(QVector<double>);
	void updateSerial(QVector<double>);
	void printError(QString error);

public:
	Serial *mySerial;
	Controller *Ctrl;
	QTimer *timer;
private:
	Ui::MainWindow *ui;
	QwtPlot      *plot;
	QwtPlotCurve *curve;
	static const int plotDataSize = 100;
	double xData[plotDataSize];
	double yData[plotDataSize];
	
};

#endif // MAINWINDOW_H
